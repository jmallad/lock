#include "error.h"

const char* spinlock_strerror(int err) {
	switch(err) {
	case ENULLPTR:  return "Null pointer dereference";
	case EALLOC:    return "Allocation failure (Are you out of RAM?)";
	case EFP:       return "Floating-point error";
	case EFIO:      return "File I/O error";
	case EBUFOV:    return "Buffer over/underflow";
	case EEOF:      return "Unexpected end-of-file";
	case ELOCK:     return "Failed to acquire lock";
		/* Warnings */
	case ETODO:     return "Not yet implemented";
	case EARGMIN:   return "Not enough arguments (see \"help\")";
	case EOPEN:     return "File open failed";
	}
	return "Unrecognized error code";
}
