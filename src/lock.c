#include <stdlib.h>
#include <stdatomic.h>
#include <sched.h>
#include <string.h>
#include <threads.h>
#include <stdarg.h>
#include "lock.h"
#include "error.h"

#define FILENAME "lock.c"

/****** Static functions ******/

static int lock(pthread_mutex_t* lock)
{
	if (!lock) {
		return ENULLPTR;
	}
	return pthread_mutex_lock(lock);
}
static int unlock(pthread_mutex_t* lock)
{
	if (!lock) {
		return ENULLPTR;
	}
	return pthread_mutex_unlock(lock);
}
static int try_lock(pthread_mutex_t* lock)       
{
	if (!lock) {
		return ENULLPTR;
	}
	return pthread_mutex_trylock(lock);
}
/* Allocate and initialize a new write lock structure */
int new_lock(struct lock** p)
{
	int ret;
	if (!p) {
		return ENULLPTR;
	}
	*p = malloc(sizeof(struct lock));
	if (!*p) {
		return EALLOC;
	}
	memset(*p, 0, sizeof(struct lock));
	ret = pthread_mutex_init(&(*p)->lock, NULL);
	if (ret) {
		return ret;
	}
	return 0;
}

/****** Global functions ******/

/* Attempt to acquire a lock for writing. Return 0 if successful. */
int write_lock(struct lock* p)
{
	int ret;
	if (!p) {
		return ENULLPTR;
	}
	ret = lock(&p->lock);
	if (ret) {
		return ELOCK;
	}
	while (p->readers) {
	}
	return 0;
}

/* Attempt to acquire a lock for writing. Return 0 if successful. */
int try_write_lock(struct lock* p)
{
	int ret;
	if (!p) {
		return ENULLPTR;
	}
	ret = try_lock(&p->lock);
	if (ret) {
		return ELOCK;
	}
	if (p->readers) {
		unlock(&p->lock);
		return ELOCK;
	}
	return 0;
}

/* Release a write lock. */
int write_unlock(struct lock* p)
{
	if (!p) {
		return ENULLPTR;
	}
	if (unlock(&p->lock)) {
		return ELOCK;
	}
	return 0;
}

/* Attempt to acquire a lock for reading. If successful, increment the reader
   count, release the lock, and return 0. */
int read_lock(struct lock* p) {
	int ret;
	if (!p) {
		return ENULLPTR;
	}
	ret = lock(&p->lock);
	/* No error check as we checked for null pointer above */
	if (ret) {
		return ELOCK;
	}
	p->readers++;
	unlock(&p->lock);
	return 0;
}

/* Attempt to acquire a lock for reading. This function differs from 
   read_lock by immediately returning if it us unable to acquire the lock
   on the first try. */
int try_read_lock(struct lock* p) {
	int ret;
	if (!p) {
		return ENULLPTR;
	}
	ret = try_lock(&p->lock);
	/* No error check as we checked for null pointer above */
	if (ret) {
		return ELOCK;
	}
	p->readers++;
	unlock(&p->lock);
	return 0;
}


/* Release a lock acquired for reading by decrementing the readers count. */
int read_unlock(struct lock* p) {
	if (!p) {
		return ENULLPTR;
	}
	p->readers--;
	return 0;
}
/* Free a write lock */
void free_lock(struct lock* p) {
	if (!p) {
		return;
	}
	pthread_mutex_destroy(&p->lock);
	memset(p, 0, sizeof(struct lock));
	free(p);
}

