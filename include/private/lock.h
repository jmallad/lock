#pragma once
#include <stdatomic.h>
#include <stdint.h>
#include <pthread.h>

struct lock {
	/* Released by readers after incrementing the reader count. 
	   Held by writers for full length of operation. */
	pthread_mutex_t lock;
	/* Write operations cannot proceed until this value reaches 0. */
	_Atomic uint16_t readers;
	/* Unique identifier of this lock */
	uint64_t id;
};

/* Allocate a new lock structure. */
int new_lock(struct lock** p);
/* Attempt to acquire a lock for writing. Return 0 if successful. */
int write_lock(struct lock* p);
/* Attempt to acquire a lock for writing. Immediately return 1 if unable
   to acquire the lock on the first try. Return 0 if successful. */
int try_write_lock(struct lock* p);
/* Release a write lock. */
int write_unlock(struct lock* p);
/* Attempt to acquire a lock for reading. If successful, increment the reader  
   count, release the lock, and return 0. */
int read_lock(struct lock* p);
/* Attempt to acquire a lock for reading. Immediately return 1 if unable
   to acquire the lock on the first try. Return 0 if successful. */
int try_read_lock(struct lock* p);
/* Release a lock acquired for reading by decrementing the readers count. */
int read_unlock(struct lock* p);
/* Free a lock */
void free_lock(struct lock* p);
