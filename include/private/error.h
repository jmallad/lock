#pragma once

enum {
	SUCCESS   =  0, /* No error */
	ENULLPTR  = -1, /* Null pointer dereference */
	EALLOC    = -2, /* Allocation failure */
	EFP       = -3, /* Floating-point error */
	EFIO      = -5, /* File I/O Error */
	EBUFOV    = -6, /* Buffer over/underflow */
	EEOF      = -7, /* Unexpected EOF */
	ELOCK     = -8, /* Failed to acquire lock */
	/* All codes below this point are non-terminating warnings */
	EWARNING  = -100,
	ETODO     = -101, /* Not Yet Implemented */
	EARGMIN   = -102, /* Not enough arguments (see "help") */
	EOPEN     = -103  /* File open failed */
};

const char* spinlock_strerror(int err);
